<?php

namespace Tasko\TaskoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TaskoUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('password')
            ->add('nickname')
            ->add('lastAccess')
            ->add('active')
            ->add('role')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tasko\TaskoBundle\Entity\TaskoUser'
        ));
    }

    public function getName()
    {
        return 'tasko_taskobundle_taskousertype';
    }
}
