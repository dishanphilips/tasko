<?php

namespace Tasko\TaskoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TaskoTechnologyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('technologyName')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tasko\TaskoBundle\Entity\TaskoTechnology'
        ));
    }

    public function getName()
    {
        return 'tasko_taskobundle_taskotechnologytype';
    }
}
