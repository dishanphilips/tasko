<?php

namespace Tasko\TaskoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TaskoProjectTechnologyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('project')
            ->add('technology')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tasko\TaskoBundle\Entity\TaskoProjectTechnology'
        ));
    }

    public function getName()
    {
        return 'tasko_taskobundle_taskoprojecttechnologytype';
    }
}
