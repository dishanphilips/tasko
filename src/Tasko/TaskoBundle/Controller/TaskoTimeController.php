<?php

namespace Tasko\TaskoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Tasko\TaskoBundle\Entity\TaskoTime;
use Tasko\TaskoBundle\Form\TaskoTimeType;

/**
 * TaskoTime controller.
 *
 */
class TaskoTimeController extends Controller
{

    /**
     * Lists all TaskoTime entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TaskoBundle:TaskoTime')->findAll();

        return $this->render('TaskoBundle:TaskoTime:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TaskoTime entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new TaskoTime();
        $form = $this->createForm(new TaskoTimeType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('time_show', array('id' => $entity->getId())));
        }

        return $this->render('TaskoBundle:TaskoTime:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new TaskoTime entity.
     *
     */
    public function newAction()
    {
        $entity = new TaskoTime();
        $form   = $this->createForm(new TaskoTimeType(), $entity);

        return $this->render('TaskoBundle:TaskoTime:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TaskoTime entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TaskoBundle:TaskoTime')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TaskoTime entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TaskoBundle:TaskoTime:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing TaskoTime entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TaskoBundle:TaskoTime')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TaskoTime entity.');
        }

        $editForm = $this->createForm(new TaskoTimeType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TaskoBundle:TaskoTime:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing TaskoTime entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TaskoBundle:TaskoTime')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TaskoTime entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new TaskoTimeType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('time_edit', array('id' => $id)));
        }

        return $this->render('TaskoBundle:TaskoTime:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TaskoTime entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TaskoBundle:TaskoTime')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TaskoTime entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('time'));
    }

    /**
     * Creates a form to delete a TaskoTime entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
