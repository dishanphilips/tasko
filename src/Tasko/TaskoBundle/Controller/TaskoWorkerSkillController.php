<?php

namespace Tasko\TaskoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Tasko\TaskoBundle\Entity\TaskoWorkerSkill;
use Tasko\TaskoBundle\Form\TaskoWorkerSkillType;

/**
 * TaskoWorkerSkill controller.
 *
 */
class TaskoWorkerSkillController extends Controller
{

    /**
     * Lists all TaskoWorkerSkill entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TaskoBundle:TaskoWorkerSkill')->findAll();

        return $this->render('TaskoBundle:TaskoWorkerSkill:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TaskoWorkerSkill entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new TaskoWorkerSkill();
        $form = $this->createForm(new TaskoWorkerSkillType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('worker-skills_show', array('id' => $entity->getId())));
        }

        return $this->render('TaskoBundle:TaskoWorkerSkill:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new TaskoWorkerSkill entity.
     *
     */
    public function newAction()
    {
        $entity = new TaskoWorkerSkill();
        $form   = $this->createForm(new TaskoWorkerSkillType(), $entity);

        return $this->render('TaskoBundle:TaskoWorkerSkill:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TaskoWorkerSkill entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TaskoBundle:TaskoWorkerSkill')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TaskoWorkerSkill entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TaskoBundle:TaskoWorkerSkill:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing TaskoWorkerSkill entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TaskoBundle:TaskoWorkerSkill')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TaskoWorkerSkill entity.');
        }

        $editForm = $this->createForm(new TaskoWorkerSkillType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TaskoBundle:TaskoWorkerSkill:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing TaskoWorkerSkill entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TaskoBundle:TaskoWorkerSkill')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TaskoWorkerSkill entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new TaskoWorkerSkillType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('worker-skills_edit', array('id' => $id)));
        }

        return $this->render('TaskoBundle:TaskoWorkerSkill:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TaskoWorkerSkill entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TaskoBundle:TaskoWorkerSkill')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TaskoWorkerSkill entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('worker-skills'));
    }

    /**
     * Creates a form to delete a TaskoWorkerSkill entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
