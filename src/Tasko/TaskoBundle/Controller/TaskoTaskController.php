<?php

namespace Tasko\TaskoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Tasko\TaskoBundle\Entity\TaskoTask;
use Tasko\TaskoBundle\Form\TaskoTaskType;

/**
 * TaskoTask controller.
 *
 */
class TaskoTaskController extends Controller
{

    /**
     * Lists all TaskoTask entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TaskoBundle:TaskoTask')->findAll();

        return $this->render('TaskoBundle:TaskoTask:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TaskoTask entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new TaskoTask();
        $form = $this->createForm(new TaskoTaskType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tasks_show', array('id' => $entity->getId())));
        }

        return $this->render('TaskoBundle:TaskoTask:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new TaskoTask entity.
     *
     */
    public function newAction()
    {
        $entity = new TaskoTask();
        $form   = $this->createForm(new TaskoTaskType(), $entity);

        return $this->render('TaskoBundle:TaskoTask:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TaskoTask entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TaskoBundle:TaskoTask')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TaskoTask entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TaskoBundle:TaskoTask:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing TaskoTask entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TaskoBundle:TaskoTask')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TaskoTask entity.');
        }

        $editForm = $this->createForm(new TaskoTaskType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TaskoBundle:TaskoTask:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing TaskoTask entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TaskoBundle:TaskoTask')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TaskoTask entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new TaskoTaskType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tasks_edit', array('id' => $id)));
        }

        return $this->render('TaskoBundle:TaskoTask:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TaskoTask entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TaskoBundle:TaskoTask')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TaskoTask entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tasks'));
    }

    /**
     * Creates a form to delete a TaskoTask entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
