<?php

namespace Tasko\TaskoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TaskoStatisticController extends Controller
{
    public function indexAction()
    {
        return $this->render('TaskoBundle:TaskoStatistic:index.html.twig');
    }
}