<?php

namespace Tasko\TaskoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Tasko\TaskoBundle\Entity\TaskoTechnology;
use Tasko\TaskoBundle\Form\TaskoTechnologyType;

/**
 * TaskoTechnology controller.
 *
 */
class TaskoTechnologyController extends Controller
{

    /**
     * Lists all TaskoTechnology entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TaskoBundle:TaskoTechnology')->findAll();

        return $this->render('TaskoBundle:TaskoTechnology:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TaskoTechnology entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new TaskoTechnology();
        $form = $this->createForm(new TaskoTechnologyType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('technologies_show', array('id' => $entity->getId())));
        }

        return $this->render('TaskoBundle:TaskoTechnology:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new TaskoTechnology entity.
     *
     */
    public function newAction()
    {
        $entity = new TaskoTechnology();
        $form   = $this->createForm(new TaskoTechnologyType(), $entity);

        return $this->render('TaskoBundle:TaskoTechnology:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TaskoTechnology entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TaskoBundle:TaskoTechnology')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TaskoTechnology entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TaskoBundle:TaskoTechnology:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing TaskoTechnology entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TaskoBundle:TaskoTechnology')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TaskoTechnology entity.');
        }

        $editForm = $this->createForm(new TaskoTechnologyType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TaskoBundle:TaskoTechnology:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing TaskoTechnology entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TaskoBundle:TaskoTechnology')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TaskoTechnology entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new TaskoTechnologyType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('technologies_edit', array('id' => $id)));
        }

        return $this->render('TaskoBundle:TaskoTechnology:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TaskoTechnology entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TaskoBundle:TaskoTechnology')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TaskoTechnology entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('technologies'));
    }

    /**
     * Creates a form to delete a TaskoTechnology entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
