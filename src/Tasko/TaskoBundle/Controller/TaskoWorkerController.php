<?php

namespace Tasko\TaskoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Tasko\TaskoBundle\Entity\TaskoWorker;
use Tasko\TaskoBundle\Form\TaskoWorkerType;

/**
 * TaskoWorker controller.
 *
 */
class TaskoWorkerController extends Controller
{

    /**
     * Lists all TaskoWorker entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TaskoBundle:TaskoWorker')->findAll();

        return $this->render('TaskoBundle:TaskoWorker:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TaskoWorker entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new TaskoWorker();
        $form = $this->createForm(new TaskoWorkerType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('workers_show', array('id' => $entity->getId())));
        }

        return $this->render('TaskoBundle:TaskoWorker:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new TaskoWorker entity.
     *
     */
    public function newAction()
    {
        $entity = new TaskoWorker();
        $form   = $this->createForm(new TaskoWorkerType(), $entity);

        return $this->render('TaskoBundle:TaskoWorker:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TaskoWorker entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TaskoBundle:TaskoWorker')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TaskoWorker entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TaskoBundle:TaskoWorker:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing TaskoWorker entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TaskoBundle:TaskoWorker')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TaskoWorker entity.');
        }

        $editForm = $this->createForm(new TaskoWorkerType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TaskoBundle:TaskoWorker:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing TaskoWorker entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TaskoBundle:TaskoWorker')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TaskoWorker entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new TaskoWorkerType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('workers_edit', array('id' => $id)));
        }

        return $this->render('TaskoBundle:TaskoWorker:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TaskoWorker entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TaskoBundle:TaskoWorker')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TaskoWorker entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('workers'));
    }

    /**
     * Creates a form to delete a TaskoWorker entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
