<?php

namespace Tasko\TaskoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('TaskoBundle:Default:index.html.twig');
    }
}
