<?php

namespace Tasko\TaskoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TaskoConfigurationController extends Controller
{
    public function indexAction()
    {
        return $this->render('TaskoBundle:TaskoConfiguration:index.html.twig');
    }
}