<?php

namespace Tasko\TaskoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Tasko\TaskoBundle\Entity\TaskoProjectTechnology;
use Tasko\TaskoBundle\Form\TaskoProjectTechnologyType;

/**
 * TaskoProjectTechnology controller.
 *
 */
class TaskoProjectTechnologyController extends Controller
{

    /**
     * Lists all TaskoProjectTechnology entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TaskoBundle:TaskoProjectTechnology')->findAll();

        return $this->render('TaskoBundle:TaskoProjectTechnology:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TaskoProjectTechnology entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new TaskoProjectTechnology();
        $form = $this->createForm(new TaskoProjectTechnologyType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('project-technologies_show', array('id' => $entity->getId())));
        }

        return $this->render('TaskoBundle:TaskoProjectTechnology:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new TaskoProjectTechnology entity.
     *
     */
    public function newAction()
    {
        $entity = new TaskoProjectTechnology();
        $form   = $this->createForm(new TaskoProjectTechnologyType(), $entity);

        return $this->render('TaskoBundle:TaskoProjectTechnology:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TaskoProjectTechnology entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TaskoBundle:TaskoProjectTechnology')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TaskoProjectTechnology entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TaskoBundle:TaskoProjectTechnology:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing TaskoProjectTechnology entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TaskoBundle:TaskoProjectTechnology')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TaskoProjectTechnology entity.');
        }

        $editForm = $this->createForm(new TaskoProjectTechnologyType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TaskoBundle:TaskoProjectTechnology:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing TaskoProjectTechnology entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TaskoBundle:TaskoProjectTechnology')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TaskoProjectTechnology entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new TaskoProjectTechnologyType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('project-technologies_edit', array('id' => $id)));
        }

        return $this->render('TaskoBundle:TaskoProjectTechnology:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TaskoProjectTechnology entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TaskoBundle:TaskoProjectTechnology')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TaskoProjectTechnology entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('project-technologies'));
    }

    /**
     * Creates a form to delete a TaskoProjectTechnology entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
