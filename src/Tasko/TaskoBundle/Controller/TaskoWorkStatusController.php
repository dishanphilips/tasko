<?php

namespace Tasko\TaskoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Tasko\TaskoBundle\Entity\TaskoWorkStatus;
use Tasko\TaskoBundle\Form\TaskoWorkStatusType;

/**
 * TaskoWorkStatus controller.
 *
 */
class TaskoWorkStatusController extends Controller
{

    /**
     * Lists all TaskoWorkStatus entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TaskoBundle:TaskoWorkStatus')->findAll();

        return $this->render('TaskoBundle:TaskoWorkStatus:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TaskoWorkStatus entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new TaskoWorkStatus();
        $form = $this->createForm(new TaskoWorkStatusType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('work-status_show', array('id' => $entity->getId())));
        }

        return $this->render('TaskoBundle:TaskoWorkStatus:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new TaskoWorkStatus entity.
     *
     */
    public function newAction()
    {
        $entity = new TaskoWorkStatus();
        $form   = $this->createForm(new TaskoWorkStatusType(), $entity);

        return $this->render('TaskoBundle:TaskoWorkStatus:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TaskoWorkStatus entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TaskoBundle:TaskoWorkStatus')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TaskoWorkStatus entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TaskoBundle:TaskoWorkStatus:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing TaskoWorkStatus entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TaskoBundle:TaskoWorkStatus')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TaskoWorkStatus entity.');
        }

        $editForm = $this->createForm(new TaskoWorkStatusType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TaskoBundle:TaskoWorkStatus:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing TaskoWorkStatus entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TaskoBundle:TaskoWorkStatus')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TaskoWorkStatus entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new TaskoWorkStatusType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('work-status_edit', array('id' => $id)));
        }

        return $this->render('TaskoBundle:TaskoWorkStatus:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TaskoWorkStatus entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TaskoBundle:TaskoWorkStatus')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TaskoWorkStatus entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('work-status'));
    }

    /**
     * Creates a form to delete a TaskoWorkStatus entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
