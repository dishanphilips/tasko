<?php

namespace Tasko\TaskoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Tasko\TaskoBundle\Entity\TaskoProject;
use Tasko\TaskoBundle\Form\TaskoProjectType;

/**
 * TaskoProject controller.
 *
 */
class TaskoProjectController extends Controller
{

    /**
     * Lists all TaskoProject entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TaskoBundle:TaskoProject')->findAll();

        return $this->render('TaskoBundle:TaskoProject:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TaskoProject entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new TaskoProject();
        $form = $this->createForm(new TaskoProjectType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('projects_show', array('id' => $entity->getId())));
        }

        return $this->render('TaskoBundle:TaskoProject:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new TaskoProject entity.
     *
     */
    public function newAction()
    {
        $entity = new TaskoProject();
        $form   = $this->createForm(new TaskoProjectType(), $entity);

        return $this->render('TaskoBundle:TaskoProject:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TaskoProject entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TaskoBundle:TaskoProject')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TaskoProject entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TaskoBundle:TaskoProject:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing TaskoProject entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TaskoBundle:TaskoProject')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TaskoProject entity.');
        }

        $editForm = $this->createForm(new TaskoProjectType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TaskoBundle:TaskoProject:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing TaskoProject entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TaskoBundle:TaskoProject')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TaskoProject entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new TaskoProjectType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('projects_edit', array('id' => $id)));
        }

        return $this->render('TaskoBundle:TaskoProject:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TaskoProject entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TaskoBundle:TaskoProject')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TaskoProject entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('projects'));
    }

    /**
     * Creates a form to delete a TaskoProject entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
