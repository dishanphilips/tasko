<?php

namespace Tasko\TaskoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TaskoProjectTechnology
 */
class TaskoProjectTechnology
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Tasko\TaskoBundle\Entity\TaskoProject
     */
    private $project;

    /**
     * @var \Tasko\TaskoBundle\Entity\TaskoTechnology
     */
    private $technology;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set project
     *
     * @param \Tasko\TaskoBundle\Entity\TaskoProject $project
     * @return TaskoProjectTechnology
     */
    public function setProject(\Tasko\TaskoBundle\Entity\TaskoProject $project = null)
    {
        $this->project = $project;
    
        return $this;
    }

    /**
     * Get project
     *
     * @return \Tasko\TaskoBundle\Entity\TaskoProject 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set technology
     *
     * @param \Tasko\TaskoBundle\Entity\TaskoTechnology $technology
     * @return TaskoProjectTechnology
     */
    public function setTechnology(\Tasko\TaskoBundle\Entity\TaskoTechnology $technology = null)
    {
        $this->technology = $technology;
    
        return $this;
    }

    /**
     * Get technology
     *
     * @return \Tasko\TaskoBundle\Entity\TaskoTechnology 
     */
    public function getTechnology()
    {
        return $this->technology;
    }
}
