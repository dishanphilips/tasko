<?php

namespace Tasko\TaskoBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * TaskoUser
 */
class TaskoUser implements UserInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $salt;

    /**
     * @var string
     */
    protected $nickname;

    /**
     * @var \DateTime
     */
    protected $lastAccess;

    /**
     * @var boolean
     */
    protected $active;

    /**
     * @var \Tasko\TaskoBundle\Entity\TaskoRole
     */
    protected $role;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return TaskoUser
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return TaskoUser
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return TaskoUser
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    
        return $this;
    }

    /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set nickname
     *
     * @param string $nickname
     * @return TaskoUser
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;
    
        return $this;
    }

    /**
     * Get nickname
     *
     * @return string 
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * Set lastAccess
     *
     * @param \DateTime $lastAccess
     * @return TaskoUser
     */
    public function setLastAccess($lastAccess)
    {
        $this->lastAccess = $lastAccess;
    
        return $this;
    }

    /**
     * Get lastAccess
     *
     * @return \DateTime 
     */
    public function getLastAccess()
    {
        return $this->lastAccess;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return TaskoUser
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set role
     *
     * @param \Tasko\TaskoBundle\Entity\TaskoRole $role
     * @return TaskoUser
     */
    public function setRole(\Tasko\TaskoBundle\Entity\TaskoRole $role = null)
    {
        $this->role = $role;
    
        return $this;
    }

    /**
     * Get role
     *
     * @return \Tasko\TaskoBundle\Entity\TaskoRole 
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Get roles
     *
     * @return \Tasko\TaskoBundle\Entity\TaskoRole
     */
    public function getRoles()
    {
        return array($this->role);
    }

    /**
     * Erase Credentials
     *
     */
    public function eraseCredentials()
    {
        $this->password = null;
        $this->salt = null;
    }

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize($this->id);
    }

    /**
     * @param string $data
     */
    public function unserialize($data)
    {
        $this->id = unserialize($data);
    }

    public function __sleep()
    {
        return array('id');
    }
}
