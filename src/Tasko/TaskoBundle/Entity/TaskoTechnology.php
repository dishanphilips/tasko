<?php

namespace Tasko\TaskoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TaskoTechnology
 */
class TaskoTechnology
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $technologyName;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set technologyName
     *
     * @param string $technologyName
     * @return TaskoTechnology
     */
    public function setTechnologyName($technologyName)
    {
        $this->technologyName = $technologyName;
    
        return $this;
    }

    /**
     * Get technologyName
     *
     * @return string 
     */
    public function getTechnologyName()
    {
        return $this->technologyName;
    }
}
