<?php

namespace Tasko\TaskoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TaskoWorker
 */
class TaskoWorker
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Tasko\TaskoBundle\Entity\TaskoTask
     */
    private $worker;

    /**
     * @var \Tasko\TaskoBundle\Entity\TaskoUser
     */
    private $task;

    /**
     * @var \Tasko\TaskoBundle\Entity\TaskoWorkStatus
     */
    private $workStatus;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set worker
     *
     * @param \Tasko\TaskoBundle\Entity\TaskoTask $worker
     * @return TaskoWorker
     */
    public function setWorker(\Tasko\TaskoBundle\Entity\TaskoTask $worker = null)
    {
        $this->worker = $worker;
    
        return $this;
    }

    /**
     * Get worker
     *
     * @return \Tasko\TaskoBundle\Entity\TaskoTask 
     */
    public function getWorker()
    {
        return $this->worker;
    }

    /**
     * Set task
     *
     * @param \Tasko\TaskoBundle\Entity\TaskoUser $task
     * @return TaskoWorker
     */
    public function setTask(\Tasko\TaskoBundle\Entity\TaskoUser $task = null)
    {
        $this->task = $task;
    
        return $this;
    }

    /**
     * Get task
     *
     * @return \Tasko\TaskoBundle\Entity\TaskoUser 
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set workStatus
     *
     * @param \Tasko\TaskoBundle\Entity\TaskoWorkStatus $workStatus
     * @return TaskoWorker
     */
    public function setWorkStatus(\Tasko\TaskoBundle\Entity\TaskoWorkStatus $workStatus = null)
    {
        $this->workStatus = $workStatus;
    
        return $this;
    }

    /**
     * Get workStatus
     *
     * @return \Tasko\TaskoBundle\Entity\TaskoWorkStatus 
     */
    public function getWorkStatus()
    {
        return $this->workStatus;
    }
}
