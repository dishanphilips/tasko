<?php

namespace Tasko\TaskoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TaskoWorkerSkill
 */
class TaskoWorkerSkill
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Tasko\TaskoBundle\Entity\TaskoTechnology
     */
    private $technology;

    /**
     * @var \Tasko\TaskoBundle\Entity\TaskoUser
     */
    private $worker;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set technology
     *
     * @param \Tasko\TaskoBundle\Entity\TaskoTechnology $technology
     * @return TaskoWorkerSkill
     */
    public function setTechnology(\Tasko\TaskoBundle\Entity\TaskoTechnology $technology = null)
    {
        $this->technology = $technology;
    
        return $this;
    }

    /**
     * Get technology
     *
     * @return \Tasko\TaskoBundle\Entity\TaskoTechnology 
     */
    public function getTechnology()
    {
        return $this->technology;
    }

    /**
     * Set worker
     *
     * @param \Tasko\TaskoBundle\Entity\TaskoUser $worker
     * @return TaskoWorkerSkill
     */
    public function setWorker(\Tasko\TaskoBundle\Entity\TaskoUser $worker = null)
    {
        $this->worker = $worker;
    
        return $this;
    }

    /**
     * Get worker
     *
     * @return \Tasko\TaskoBundle\Entity\TaskoUser 
     */
    public function getWorker()
    {
        return $this->worker;
    }
}
