<?php

namespace Tasko\TaskoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TaskoTask
 */
class TaskoTask
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $taskName;

    /**
     * @var string
     */
    private $taskDescription;

    /**
     * @var integer
     */
    private $project;

    /**
     * @var boolean
     */
    private $billable;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set taskName
     *
     * @param string $taskName
     * @return TaskoTask
     */
    public function setTaskName($taskName)
    {
        $this->taskName = $taskName;
    
        return $this;
    }

    /**
     * Get taskName
     *
     * @return string 
     */
    public function getTaskName()
    {
        return $this->taskName;
    }

    /**
     * Set taskDescription
     *
     * @param string $taskDescription
     * @return TaskoTask
     */
    public function setTaskDescription($taskDescription)
    {
        $this->taskDescription = $taskDescription;
    
        return $this;
    }

    /**
     * Get taskDescription
     *
     * @return string 
     */
    public function getTaskDescription()
    {
        return $this->taskDescription;
    }

    /**
     * Set project
     *
     * @param integer $project
     * @return TaskoTask
     */
    public function setProject($project)
    {
        $this->project = $project;
    
        return $this;
    }

    /**
     * Get project
     *
     * @return integer 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set billable
     *
     * @param boolean $billable
     * @return TaskoTask
     */
    public function setBillable($billable)
    {
        $this->billable = $billable;
    
        return $this;
    }

    /**
     * Get billable
     *
     * @return boolean 
     */
    public function getBillable()
    {
        return $this->billable;
    }
}
