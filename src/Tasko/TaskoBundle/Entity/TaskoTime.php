<?php

namespace Tasko\TaskoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TaskoTime
 */
class TaskoTime
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $loggedTime;

    /**
     * @var \DateTime
     */
    private $logDate;

    /**
     * @var \Tasko\TaskoBundle\Entity\TaskoWorker
     */
    private $worker;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set loggedTime
     *
     * @param integer $loggedTime
     * @return TaskoTime
     */
    public function setLoggedTime($loggedTime)
    {
        $this->loggedTime = $loggedTime;
    
        return $this;
    }

    /**
     * Get loggedTime
     *
     * @return integer 
     */
    public function getLoggedTime()
    {
        return $this->loggedTime;
    }

    /**
     * Set logDate
     *
     * @param \DateTime $logDate
     * @return TaskoTime
     */
    public function setLogDate($logDate)
    {
        $this->logDate = $logDate;
    
        return $this;
    }

    /**
     * Get logDate
     *
     * @return \DateTime 
     */
    public function getLogDate()
    {
        return $this->logDate;
    }

    /**
     * Set worker
     *
     * @param \Tasko\TaskoBundle\Entity\TaskoWorker $worker
     * @return TaskoTime
     */
    public function setWorker(\Tasko\TaskoBundle\Entity\TaskoWorker $worker = null)
    {
        $this->worker = $worker;
    
        return $this;
    }

    /**
     * Get worker
     *
     * @return \Tasko\TaskoBundle\Entity\TaskoWorker 
     */
    public function getWorker()
    {
        return $this->worker;
    }
}
