<?php

namespace Tasko\TaskoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TaskoProject
 */
class TaskoProject
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $projectName;

    /**
     * @var string
     */
    private $projectDescription;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var string
     */
    private $customer;

    /**
     * @var \DateTime
     */
    private $started;

    /**
     * @var boolean
     */
    private $billable;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set projectName
     *
     * @param string $projectName
     * @return TaskoProject
     */
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;
    
        return $this;
    }

    /**
     * Get projectName
     *
     * @return string 
     */
    public function getProjectName()
    {
        return $this->projectName;
    }

    /**
     * Set projectDescription
     *
     * @param string $projectDescription
     * @return TaskoProject
     */
    public function setProjectDescription($projectDescription)
    {
        $this->projectDescription = $projectDescription;
    
        return $this;
    }

    /**
     * Get projectDescription
     *
     * @return string 
     */
    public function getProjectDescription()
    {
        return $this->projectDescription;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return TaskoProject
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set customer
     *
     * @param string $customer
     * @return TaskoProject
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    
        return $this;
    }

    /**
     * Get customer
     *
     * @return string 
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set started
     *
     * @param \DateTime $started
     * @return TaskoProject
     */
    public function setStarted($started)
    {
        $this->started = $started;
    
        return $this;
    }

    /**
     * Get started
     *
     * @return \DateTime 
     */
    public function getStarted()
    {
        return $this->started;
    }

    /**
     * Set billable
     *
     * @param boolean $billable
     * @return TaskoProject
     */
    public function setBillable($billable)
    {
        $this->billable = $billable;
    
        return $this;
    }

    /**
     * Get billable
     *
     * @return boolean 
     */
    public function getBillable()
    {
        return $this->billable;
    }
}
