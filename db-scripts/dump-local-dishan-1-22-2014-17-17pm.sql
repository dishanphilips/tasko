-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 22, 2014 at 05:16 PM
-- Server version: 5.5.29
-- PHP Version: 5.4.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `tasko`
--

-- --------------------------------------------------------

--
-- Table structure for table `tasko_project`
--

CREATE TABLE `tasko_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(200) NOT NULL,
  `project_description` varchar(200) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `customer` varchar(200) DEFAULT NULL,
  `started` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `billable` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tasko_project_technology`
--

CREATE TABLE `tasko_project_technology` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project` int(11) NOT NULL,
  `technology` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tasko_project_technology_project_fk` (`project`),
  KEY `tasko_project_technology_technology_fk` (`technology`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tasko_role`
--

CREATE TABLE `tasko_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tasko_role`
--

INSERT INTO `tasko_role` (`id`, `name`) VALUES
(1, 'ROLE_USER'),
(2, 'ROLE_ADMIN');

-- --------------------------------------------------------

--
-- Table structure for table `tasko_task`
--

CREATE TABLE `tasko_task` (
  `id` int(11) NOT NULL,
  `task_name` varchar(200) NOT NULL,
  `task_description` varchar(400) DEFAULT NULL,
  `project` int(11) NOT NULL,
  `billable` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tasko_technology`
--

CREATE TABLE `tasko_technology` (
  `id` int(11) NOT NULL,
  `technology_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tasko_time`
--

CREATE TABLE `tasko_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `worker` int(11) NOT NULL,
  `logged_time` int(11) NOT NULL,
  `log_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `tasko_time_worker_fk` (`worker`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tasko_user`
--

CREATE TABLE `tasko_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `nickname` varchar(200) NOT NULL,
  `last_access` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tasko_user_role_fk` (`role`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tasko_user`
--

INSERT INTO `tasko_user` (`id`, `username`, `password`, `salt`, `nickname`, `last_access`, `active`, `role`) VALUES
(1, 'admin', 'icBhTFbqxuaFVS69s2tUEAd0m4hoAztKUf5DRCs6la9FdUiNvva8FslACtPSmDlTzxy0f28B9e71MbEke0VkPQ==', 'hello', 'Administrator', '2014-01-22 11:45:05', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tasko_worker`
--

CREATE TABLE `tasko_worker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task` int(11) NOT NULL,
  `worker` int(11) NOT NULL,
  `work_status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tasko_worker_task_fk` (`task`),
  KEY `tasko_worker_fk` (`worker`),
  KEY `tasko_worker_work_status_fk` (`work_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tasko_worker_skill`
--

CREATE TABLE `tasko_worker_skill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `worker` int(11) NOT NULL,
  `technology` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tasko_worker_skill_worker_fk` (`worker`),
  KEY `tasko_worker_skill_technology_fk` (`technology`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tasko_work_status`
--

CREATE TABLE `tasko_work_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tasko_project_technology`
--
ALTER TABLE `tasko_project_technology`
  ADD CONSTRAINT `tasko_project_technology_project_fk` FOREIGN KEY (`project`) REFERENCES `tasko_project` (`id`),
  ADD CONSTRAINT `tasko_project_technology_technology_fk` FOREIGN KEY (`technology`) REFERENCES `tasko_technology` (`id`);

--
-- Constraints for table `tasko_time`
--
ALTER TABLE `tasko_time`
  ADD CONSTRAINT `tasko_time_worker_fk` FOREIGN KEY (`worker`) REFERENCES `tasko_worker` (`id`);

--
-- Constraints for table `tasko_user`
--
ALTER TABLE `tasko_user`
  ADD CONSTRAINT `tasko_user_role_fk` FOREIGN KEY (`role`) REFERENCES `tasko_role` (`id`);

--
-- Constraints for table `tasko_worker`
--
ALTER TABLE `tasko_worker`
  ADD CONSTRAINT `tasko_worker_fk` FOREIGN KEY (`worker`) REFERENCES `tasko_task` (`id`),
  ADD CONSTRAINT `tasko_worker_task_fk` FOREIGN KEY (`task`) REFERENCES `tasko_user` (`id`),
  ADD CONSTRAINT `tasko_worker_work_status_fk` FOREIGN KEY (`work_status`) REFERENCES `tasko_work_status` (`id`);

--
-- Constraints for table `tasko_worker_skill`
--
ALTER TABLE `tasko_worker_skill`
  ADD CONSTRAINT `tasko_worker_skill_technology_fk` FOREIGN KEY (`technology`) REFERENCES `tasko_technology` (`id`),
  ADD CONSTRAINT `tasko_worker_skill_worker_fk` FOREIGN KEY (`worker`) REFERENCES `tasko_user` (`id`);
